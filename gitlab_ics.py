#!/usr/bin/python

import os
import gitlab
import icalendar
from datetime import datetime
import pytz
import uuid

def create_ics(name, description, user, due_date, created_date):
    calendar = icalendar.Calendar()
    calendar.add('prodid', 'SlantTech Gitlab Script')
    calendar.add('version', '2.0')
    
    todo = icalendar.Todo()
    #uid required for radicale
    todo.add('uid', uuid.uuid4() )
    todo.add('summary', name)
    todo.add('description', description)
    todo.add('created', created_date)
    if due_date != None:
        todo.add('due', due_date)
    
    calendar.add_component(todo)
    email = user.public_email
    if email != '':
        print("Making ics file for email")
        if not os.path.exists('./tasks/' + email):
            os.makedirs('./tasks/' + email)
        ics_name = './tasks/' + email + '/' + name + '.ics'
        f = open(ics_name, 'wb')
        f.write(calendar.to_ical())
        f.close()
    else:
        # make directory based off of username
        print("Making ics file for username")
        if not os.path.exists('./tasks/' + user.username):
            os.makedirs('./tasks/' + user.username)
        ics_name = './tasks/' + user.username + '/' + name + '.ics'
        f = open(ics_name, 'wb')
        f.write(calendar.to_ical())
        f.close()


#Group ID for slant tech
slant_tech_gid = "13927288"

# Get private token from file
gl_token_file = open(".gl_priv_token.pem", "r")
gl_token = gl_token_file.read().replace('\n','')
#print("Token: {}".format(gl_token))

gl = gitlab.Gitlab()
gl = gitlab.Gitlab(private_token=gl_token)
gl.auth()

print("Authenticated")

# get slant-tech group
slant_tech_group = gl.groups.get(slant_tech_gid)

# list projects inside of group
projects = slant_tech_group.projects.list(all=True, iterator=True)

# list projects
#projects = gl.projects.list(iterator=True)
#for project in projects:
#    print(project.name)
    # list issues
#    issues = project.issues.list(iterator=True)
#    for issue in issues:
#        print("Issue: {}".format(issue.title))

# List issues in gitlab group
for issue in slant_tech_group.issues.list(all=True, state='opened', iterator=True):
    print("Issue:")
    print("Title: {}".format(issue.title))
    project_id = issue.project_id
    issue_prj = gl.projects.get(project_id)
    pissue = issue_prj.issues.get(issue.iid)
    print("Project: {}".format(issue_prj.name))

    assignees = pissue.assignees
    for assignee in assignees:
        assignee_tmp = gl.users.get(assignee['id'])
        print("Assignee: {}".format(assignee['name']))
#        print("Assignee Info: {}".format(assignee_tmp))
        print("Assignee email: {}".format(assignee_tmp.public_email))
        datetime_created = datetime.strptime(pissue.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
        if( pissue.due_date != None ):
            datetime_due = datetime.strptime(pissue.due_date, "%Y-%m-%d" ) 
        else:
            datetime_due = None

        create_ics(issue.title, issue.description, assignee_tmp, datetime_due, datetime_created)

#    print("Issue info:{}".format(pissue))
    print("")

print("Done")
exit()



